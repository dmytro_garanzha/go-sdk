package solidgate

type MerchantData struct {
	PaymentIntent string
	Merchant    string
	Signature     string
}